import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-task1',
  templateUrl: './task1.component.html',
  styleUrls: ['./task1.component.scss']
})
export class Task1Component implements OnInit {
TotalWord:any;
InputStrng:any;
  constructor() { }

  ngOnInit() {
  }
  count() {
    this.TotalWord = (this.InputStrng.match(/[A-Z]/g) || []).length + 1;
    
  }

}
