import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-task3',
  templateUrl: './task3.component.html',
  styleUrls: ['./task3.component.scss']
})
export class Task3Component implements OnInit {
  boxNo:any;
  constructor() { }

  ngOnInit() {
  }
  colorBox() {
if(this.boxNo == 1) {
  document.getElementById("one").style.background = "green";
  document.getElementById("two").style.background = "lightblue";
  document.getElementById("three").style.background = "lightblue";
  document.getElementById("four").style.background = "lightblue";
  document.getElementById("five").style.background = "lightblue";
  document.getElementById("six").style.background = "lightblue";
  document.getElementById("seven").style.background = "lightblue";
  document.getElementById("eight").style.background = "lightblue";
  document.getElementById("nine").style.background = "lightblue";
  
}
else if(this.boxNo == 2) {
  document.getElementById("two").style.background = "green";
  document.getElementById("one").style.background = "lightblue";
  
  document.getElementById("three").style.background = "lightblue";
  document.getElementById("four").style.background = "lightblue";
  document.getElementById("five").style.background = "lightblue";
  document.getElementById("six").style.background = "lightblue";
  document.getElementById("seven").style.background = "lightblue";
  document.getElementById("eight").style.background = "lightblue";
  document.getElementById("nine").style.background = "lightblue";
  
}
else if(this.boxNo == 3) {
  document.getElementById("three").style.background = "green";
  document.getElementById("one").style.background = "lightblue";
  document.getElementById("two").style.background = "lightblue";
  
  document.getElementById("four").style.background = "lightblue";
  document.getElementById("five").style.background = "lightblue";
  document.getElementById("six").style.background = "lightblue";
  document.getElementById("seven").style.background = "lightblue";
  document.getElementById("eight").style.background = "lightblue";
  document.getElementById("nine").style.background = "lightblue";
  
}
else if(this.boxNo == 4) {
  document.getElementById("four").style.background = "green";
  document.getElementById("one").style.background = "lightblue";
  document.getElementById("two").style.background = "lightblue";
  document.getElementById("three").style.background = "lightblue";
  
  document.getElementById("five").style.background = "lightblue";
  document.getElementById("six").style.background = "lightblue";
  document.getElementById("seven").style.background = "lightblue";
  document.getElementById("eight").style.background = "lightblue";
  document.getElementById("nine").style.background = "lightblue";
  
}
else if(this.boxNo == 5) {
  document.getElementById("five").style.background = "green";
  document.getElementById("one").style.background = "lightblue";
  document.getElementById("two").style.background = "lightblue";
  document.getElementById("three").style.background = "lightblue";
  document.getElementById("four").style.background = "lightblue";
  
  document.getElementById("six").style.background = "lightblue";
  document.getElementById("seven").style.background = "lightblue";
  document.getElementById("eight").style.background = "lightblue";
  document.getElementById("nine").style.background = "lightblue";
  
}
else if(this.boxNo == 6) {
  document.getElementById("six").style.background = "green";
  document.getElementById("one").style.background = "lightblue";
  document.getElementById("two").style.background = "lightblue";
  document.getElementById("three").style.background = "lightblue";
  document.getElementById("four").style.background = "lightblue";
  document.getElementById("five").style.background = "lightblue";
 
  document.getElementById("seven").style.background = "lightblue";
  document.getElementById("eight").style.background = "lightblue";
  document.getElementById("nine").style.background = "lightblue";
  
}
else if(this.boxNo == 7) {
  document.getElementById("seven").style.background = "green";
  document.getElementById("one").style.background = "lightblue";
  document.getElementById("two").style.background = "lightblue";
  document.getElementById("three").style.background = "lightblue";
  document.getElementById("four").style.background = "lightblue";
  document.getElementById("five").style.background = "lightblue";
  document.getElementById("six").style.background = "lightblue";
 
  document.getElementById("eight").style.background = "lightblue";
  document.getElementById("nine").style.background = "lightblue";
  
}
else if(this.boxNo == 8) {
  document.getElementById("eight").style.background = "green";
  document.getElementById("one").style.background = "lightblue";
  document.getElementById("two").style.background = "lightblue";
  document.getElementById("three").style.background = "lightblue";
  document.getElementById("four").style.background = "lightblue";
  document.getElementById("five").style.background = "lightblue";
  document.getElementById("six").style.background = "lightblue";
  document.getElementById("seven").style.background = "lightblue";
 
  document.getElementById("nine").style.background = "lightblue";
  
}
else if(this.boxNo == 9) {
  document.getElementById("nine").style.background = "green";
  document.getElementById("one").style.background = "lightblue";
  document.getElementById("two").style.background = "lightblue";
  document.getElementById("three").style.background = "lightblue";
  document.getElementById("four").style.background = "lightblue";
  document.getElementById("five").style.background = "lightblue";
  document.getElementById("six").style.background = "lightblue";
  document.getElementById("seven").style.background = "lightblue";
  document.getElementById("eight").style.background = "lightblue";
  
  
}
  }

}
